// _WindowsManager_Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "_WindowsManager_.h"
#include "_WindowsManager_Dlg.h"
#include "ProcessManager.h"
#include ".\_windowsmanager_dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CWindowsManager_Dlg dialog



CWindowsManager_Dlg::CWindowsManager_Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWindowsManager_Dlg::IDD, pParent)
	, mSizeWidth(0)
	, mSizeHeigh(0)
	, mIeUrl(_T(""))
	, mWindowTitle(_T(""))
	, m_TransparencyLevel(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWindowsManager_Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_SIZE_W, mSizeWidth);
	DDX_Text(pDX, IDC_EDIT_SIZE_H, mSizeHeigh);
	DDX_Text(pDX, IDC_EDIT_URL, mIeUrl);
	DDX_Text(pDX, IDC_EDIT_WINDOW_TITLE, mWindowTitle);
	DDX_Slider(pDX, IDC_SLIDER_TRANSPARENCY, m_TransparencyLevel);
	DDX_Control(pDX, IDC_SLIDER_TRANSPARENCY, m_Transparency);
}

BEGIN_MESSAGE_MAP(CWindowsManager_Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_LBN_DBLCLK(IDC_WINDOWSLIST, OnLbnDblclkWindowslist)
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnBnClickedButton3)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON6, OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON9, OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON10, OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON11, OnBnClickedButton11)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, OnBnClickedButtonRefresh)
	ON_BN_CLICKED(IDC_BUTTON_SET_SIZE, OnBnClickedButtonSetSize)
	ON_BN_CLICKED(IDC_BUTTON_BRINGTOFRONT_NA, OnBnClickedButtonBringtofrontNa)
	ON_BN_CLICKED(IDC_BUTTON_TO_BOTTOM, OnBnClickedButtonToBottom)
	ON_BN_CLICKED(IDC_BUTTON_NOT_ALWAYSONTOP, OnBnClickedButtonNotAlwaysontop)
	ON_BN_CLICKED(IDC_BUTTON_SET_LOCATION, OnBnClickedButtonSetLocation)
	ON_BN_CLICKED(IDC_BUTTON_SET_WINDOW_TITLE, OnBnClickedButtonSetWindowTitle)
	ON_BN_CLICKED(IDC_BUTTON_RESTORE, OnBnClickedButtonRestore)
	ON_BN_CLICKED(IDC_BUTTON_RESTORE_NOACTIVATE, OnBnClickedButtonRestoreNoactivate)
	ON_BN_CLICKED(IDC_BUTTON13, OnBnClickedButton13)
	ON_BN_CLICKED(IDC_BUTTON_SET_TRANSPARENT, OnBnClickedButtonSetTransparent)
END_MESSAGE_MAP()


// CWindowsManager_Dlg message handlers

BOOL CWindowsManager_Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	this->SetWindowPos(this, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
	m_Transparency.SetRange(0, 255);

	// Wypelnia liste okien
	RefreshWindowsList();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

BOOL CWindowsManager_Dlg::DestroyWindow()
{
	ClearWindowsData();
	

	return CDialog::DestroyWindow();
}

void CWindowsManager_Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CWindowsManager_Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CWindowsManager_Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

/**
 *	Zwraca informacje o zaznaczonym oknie.
 */
WindowInfo* CWindowsManager_Dlg::GetSelectedWindowInfo()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);
	if(l->GetCurSel() < 0)
	{
		return NULL;
	}

	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());

	return info;
}

/**
 *	Czysci dane okien przechowywane w liscie.
 */
void CWindowsManager_Dlg::ClearWindowsData()
{
	CListBox* list = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);

	if(list == NULL)
	{
		return;
	}

	void* ptr = NULL;
	for(int i = 0; i < list->GetCount(); i++)
	{
		ptr = list->GetItemDataPtr(i);
		if(ptr != NULL)
		{
			delete ptr;
		}
	}
}

void CWindowsManager_Dlg::RefreshWindowsList()
{
	vector<WindowInfo> windows = fillWindowsList();
	CListBox* list = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);

	if(list == NULL)
	{
		return;
	}

	ClearWindowsData();

	list->ResetContent();
	vector<WindowInfo>::iterator it = windows.begin();

	while(it != windows.end() )
	{
		CString data;
		data.Format("pid=%d title=\"%s\" class=\"%s\"", (*it).processId, (*it).windowTitle, 
			(*it).className);
		list->AddString(data);
		it++;
	}

	it = windows.begin();
	int i = 0;
	while(it != windows.end() )
	{
		WindowInfo* info = new WindowInfo();
		info->hwnd = (*it).hwnd;

		list->SetItemDataPtr(i++, info);
		it++;
	}
}

void CWindowsManager_Dlg::OnLbnDblclkWindowslist()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);

	if(l->GetCurSel() < 0)
	{
		return;
	}
	
	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());
	vector<WindowInfo> w = getChildWindows(info->hwnd);

	HWND h = NULL;
	if(strcmp(info->className, "Notepad") == 0 )
	{
		h = ::FindWindowEx(info->hwnd, NULL, "Edit", NULL);
	}
	else if(strcmp(info->className, "TTOTAL_CMD") == 0 )
	{
		vector<WindowInfo> ww = getChildWindows(info->hwnd);
		vector<WindowInfo>::iterator i = ww.begin();
		while(i != ww.end())
		{
			if(strcmp((*i).className, "TMyPanel") == 0)
			{
				h = (*i).hwnd;
				break;
			}
			i++;
		}
	}

	if(h != NULL)
	{
		CEdit* edit = (CEdit*)GetDlgItem(IDC_EDIT_TEXT);
		CString text;
		edit->GetWindowText(text);

		::SendMessage(h, WM_SETTEXT, NULL, (LPARAM)text.GetBuffer());
		text.ReleaseBuffer();
	}
}

void CWindowsManager_Dlg::OnBnClickedButton1()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);

	if(l->GetCurSel() < 0)
	{
		return;
	}
	
	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());
	::ShowWindow(info->hwnd, SW_MINIMIZE);
	//::ShowWindow(info->hwnd, SW_SHOWMINIMIZED);
	//::SendMessage(info->hwnd, SC_MINIMIZE, NULL, NULL);
}

void CWindowsManager_Dlg::OnBnClickedButton2()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);
	if(l->GetCurSel() < 0)
	{
		return;
	}

	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());

	::ShowWindow(info->hwnd, SW_MAXIMIZE);
}

void CWindowsManager_Dlg::OnBnClickedButton3()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);

	if(l->GetCurSel() < 0)
	{
		return;
	}

	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());

	::ShowWindow(info->hwnd, SW_HIDE);
}

void CWindowsManager_Dlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnClose();
}

void CWindowsManager_Dlg::OnBnClickedButton5()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);
	if(l->GetCurSel() < 0)
	{
		return;
	}

	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());

	::SetWindowPos(info->hwnd, NULL, 1, 1, 100, 400, NULL);
}

void CWindowsManager_Dlg::OnBnClickedButton6()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);
	if(l->GetCurSel() < 0)
	{
		return;
	}

	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());
	vector<WindowInfo> childs = getChildWindows(info->hwnd);

	vector<WindowInfo>::iterator iter = childs.begin();
	while(iter != childs.end()) {
		if(strcmp((*iter).className, "Edit") == 0) {
			break;
		}

		iter++;
	}

	if(iter != childs.end() ) {
		UpdateData();
		::SendMessage(iter->hwnd, WM_SETTEXT,NULL, (LPARAM)mIeUrl.GetBuffer());
		::SendMessage(iter->hwnd, WM_KEYDOWN, VK_RETURN, NULL);
		::SendMessage(iter->hwnd, WM_KEYUP, VK_RETURN, NULL);
	}
}

//void CWindowsManager_Dlg::OnBnClickedButton7()
//{
//	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);
//	if(l->GetCurSel() < 0)
//	{
//		return;
//	}
//
//	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());
//
//	::SetWindowLong(info->hwnd,GWL_EXSTYLE,GetWindowLong(info->hwnd,GWL_EXSTYLE)|WS_EX_LAYERED);
//	::SetLayeredWindowAttributes(info->hwnd, RGB(0, 255, 0), 255, LWA_ALPHA);
//}

//void CWindowsManager_Dlg::OnBnClickedButton8()
//{
//	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);
//	if(l->GetCurSel() < 0)
//	{
//		return;
//	}
//
//	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());
//
//	int translevel = 100;  /* 0 - 255 */
//         
//	::SetWindowLong(info->hwnd,GWL_EXSTYLE,GetWindowLong(info->hwnd,GWL_EXSTYLE)|WS_EX_LAYERED);
//	::SetLayeredWindowAttributes(info->hwnd,RGB(0,0,0),translevel, 0x02);
//	::SetWindowPos(info->hwnd, NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE);
//}

void CWindowsManager_Dlg::OnBnClickedButton9()
{

	WindowInfo* info = GetSelectedWindowInfo();

	if(info != NULL)
	{
		//SetWindowLong(info->hwnd, GWL_EXSTYLE, GetWindowLong(info->hwnd, GWL_EXSTYLE)|WS_EX_TOPMOST);
		//SetWindowLong(GetSafeHwnd(), GWL_EXSTYLE, GetWindowLong(GetSafeHwnd(), GWL_EXSTYLE)|WS_EX_TOPMOST);
		//SetWindowPos( &wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | WS_EX_TOPMOST );
		//::SetWindowPos(info->hwnd, NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | WS_EX_TOPMOST );
		//SetWindowLong(info->hwnd, GWL_EXSTYLE, SWP_NOMOVE | SWP_NOSIZE|WS_EX_TOPMOST);

		//if(info != NULL)
		//{
			::SetWindowPos(info->hwnd, HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOREDRAW);
		//}
	}
}

void CWindowsManager_Dlg::OnBnClickedButton10()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);
	if(l->GetCurSel() < 0)
	{
		return;
	}


	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());

	// to samo
	//::SetWindowPos(info->hwnd, NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	
	::SetForegroundWindow(info->hwnd);
}

void CWindowsManager_Dlg::OnBnClickedButton11()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);

	if(l->GetCurSel() < 0)
	{
		return;
	}

	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());

	::ShowWindow(info->hwnd, SW_SHOW);
}

void CWindowsManager_Dlg::OnBnClickedButtonRefresh()
{
	RefreshWindowsList();
}

void CWindowsManager_Dlg::OnBnClickedButtonSetSize()
{
	WindowInfo* info = GetSelectedWindowInfo();

	if(info != NULL)
	{
		UpdateData();
		::SetWindowPos(info->hwnd, NULL, 1, 1, mSizeWidth, mSizeHeigh, SWP_NOMOVE | SWP_NOACTIVATE);
	}
}

void CWindowsManager_Dlg::OnBnClickedButtonBringtofrontNa()
{
	WindowInfo* info = GetSelectedWindowInfo();

	if(info != NULL)
	{
		vector<WindowInfo> windows = fillWindowsList();
		WindowInfo wi = windows[0];

		//::SetWindowPos(info->hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
		//::SetWindowPos(info->hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
		//::SetWindowPos(info->hwnd, HWND_NOTOPMOST,0,0,0,0, SWP_NOACTIVATE|SWP_NOMOVE|SWP_NOSIZE|SWP_NOREDRAW);
		//::SetWindowPos(info->hwnd, 
		//	HWND_TOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
		//::SetWindowPos(info->hwnd, 
		//	HWND_NOTOPMOST,0,0,0,0, SWP_NOACTIVATE|SWP_NOMOVE|SWP_NOSIZE|SWP_NOREDRAW);
		::SetWindowPos(info->hwnd, 
			::GetForegroundWindow(), 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
	}
}

void CWindowsManager_Dlg::OnBnClickedButtonToBottom()
{
	WindowInfo* info = GetSelectedWindowInfo();

	if(info != NULL)
	{
 		::SetWindowPos(info->hwnd, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
	}
}

void CWindowsManager_Dlg::OnBnClickedButtonNotAlwaysontop()
{
	WindowInfo* info = GetSelectedWindowInfo();

	if(info != NULL)
	{
		::SetWindowPos(info->hwnd, HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOREDRAW);
	}
}

void CWindowsManager_Dlg::OnBnClickedButtonSetLocation()
{
	WindowInfo* info = GetSelectedWindowInfo();

	if(info != NULL)
	{
		UpdateData();
		::SetWindowPos(info->hwnd, NULL, mSizeWidth, mSizeHeigh, 1, 1, SWP_NOSIZE | SWP_NOACTIVATE);
	}
}

void CWindowsManager_Dlg::OnBnClickedButtonSetWindowTitle()
{
	WindowInfo* info = GetSelectedWindowInfo();

	if(info != NULL)
	{
		UpdateData();
		::SendMessage(info->hwnd, WM_SETTEXT, NULL, (LPARAM)mWindowTitle.GetBuffer());
	}
}

void CWindowsManager_Dlg::OnBnClickedButtonRestore()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);
	if(l->GetCurSel() < 0)
	{
		return;
	}

	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());

	::ShowWindow(info->hwnd, SW_RESTORE);
}

void CWindowsManager_Dlg::OnBnClickedButtonRestoreNoactivate()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);
	if(l->GetCurSel() < 0)
	{
		return;
	}

	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());

	HWND hwnd = ::GetForegroundWindow();
	::ShowWindow(info->hwnd, SW_RESTORE);
	::SetForegroundWindow(hwnd);
}

void CWindowsManager_Dlg::OnBnClickedButton13()
{
	//char* szCmd="IEXPLORE.EXE";
	//char* szParm="HTTP://GOOGLE.COM";
	//ShellExecute(NULL,"open",szCmd,szParm,NULL,SW_SHOW);
		STARTUPINFO startupInfo;
		ZeroMemory(&startupInfo, sizeof(startupInfo));
		startupInfo.cb = sizeof(startupInfo);

		startupInfo.dwFlags = /*STARTF_USESIZE |*/ STARTF_USESHOWWINDOW /*| STARTF_USEPOSITION*/;
		startupInfo.wShowWindow = SW_HIDE;
		//startupInfo.dwX = 0;
		//startupInfo.dwY = 0;
		//startupInfo.dwXSize = 10;
		//startupInfo.dwYSize = 10;

		PROCESS_INFORMATION processInformation;
		ZeroMemory(&processInformation, sizeof(processInformation));
		// Try to start the process
		BOOL result = ::CreateProcess(
							NULL,
							"\"c:\\Program Files\\Internet Explorer\\iexplore.exe\" HTTP://GOOGLE.COM",
							//"\"c:\\Programy\\Total Commander\\TOTALCMD.EXE\"",
							//"\"h:\\Moje programy\\_WindowsManager_\\_WindowsManager_\\Debug\\_WindowsManager_.exe\"",
							NULL,
							NULL,
							FALSE,
							NORMAL_PRIORITY_CLASS,
							NULL,
							NULL,
							&startupInfo,
							&processInformation
						);
		if(result == 0 )
		{
			MessageBox("Uruchamianie aplikacji nie powiodlo sie " +
				GetLastError());
		}
		else {
			try 
			{
				// czekamy az aplikacja wyswietli okno
				WaitForSingleObject(processInformation.hProcess, 1500);
				WindowInfo windowInfo = findWindow(processInformation.dwProcessId, "IEFrame");
				if(windowInfo.hwnd > 0)
				{
					::SetWindowPos(windowInfo.hwnd, NULL, 100, 100, 10, 10, SWP_SHOWWINDOW);
					//::SetWindowPos(windowInfo.hwnd, NULL, 100, 100, 10, 10, NULL);
					//::SetWindowPos(windowInfo.hwnd, NULL, 100, 100, 1000, 1000, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
					//::ShowWindow(windowInfo.hwnd, SW_SHOW);
				}
			}
			catch(Exception e) 
			{
				MessageBox(e.getMsg());
			}
		}
	//	CoInitialize(NULL);

	//IWebBrowser2* pWebBrowser = NULL;
	//HRESULT hr = CoCreateInstance(CLSID_InternetExplorer, NULL, CLSCTX_SERVER,
 //                                     IID_IWebBrowser2, (LPVOID*)&pWebBrowser);
	//VARIANT vars[4];
 //       memset(vars,0,sizeof(vars));
 //       BSTR BStrURL = _com_util::ConvertStringToBSTR((const char *)("www.google.com"));
	//
	//if (SUCCEEDED(hr) && (pWebBrowser != NULL))
	//{
	//	pWebBrowser->put_Width(600);
	//	pWebBrowser->put_Height(400);
	//	pWebBrowser->put_Left(0);
	//	pWebBrowser->put_Top(0);
	//	
	//	HRESULT hrie = pWebBrowser->Navigate(BStrURL,vars,vars+1,vars+2,vars+3);
	//	pWebBrowser->put_Visible(VARIANT_TRUE);
	//	
	//	Sleep(5000);
	//	BStrURL = _com_util::ConvertStringToBSTR((const char *)("www.onet.pl"));
	//	pWebBrowser->Navigate(BStrURL,vars,vars+1,vars+2,vars+3);
	//	Sleep(5000);
	//	pWebBrowser->Quit();
	//}
	////else
	////{
	//	if (pWebBrowser)
	//		pWebBrowser->Release();
	////}

	//CoUninitialize();
}

void CWindowsManager_Dlg::OnBnClickedButtonSetTransparent()
{
	CListBox* l = (CListBox*)GetDlgItem(IDC_WINDOWSLIST);
	if(l->GetCurSel() < 0)
	{
		return;
	}

	WindowInfo* info = (WindowInfo*)l->GetItemDataPtr(l->GetCurSel());

	UpdateData();
	::SetWindowLong(info->hwnd,GWL_EXSTYLE,GetWindowLong(info->hwnd,GWL_EXSTYLE)|WS_EX_LAYERED);
	::SetLayeredWindowAttributes(info->hwnd, RGB(0, 255, 0), m_TransparencyLevel, LWA_ALPHA);
}
