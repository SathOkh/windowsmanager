//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by _WindowsManager_.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_WINDOWSMANAGER__DIALOG      102
#define IDR_HTML_HILDWINDOWS            103
#define IDR_MAINFRAME                   128
#define IDC_WINDOWSLIST                 1001
#define IDC_BUTTON1                     1002
#define IDC_BUTTON2                     1003
#define IDC_BUTTON3                     1004
#define IDC_BUTTON4                     1005
#define IDC_BUTTON_RESTORE              1005
#define IDC_LISTCHILD                   1007
#define IDC_EDIT_TEXT                   1008
#define IDC_BUTTON5                     1009
#define IDC_BUTTON_SET_WINDOW_TITLE     1009
#define IDC_BUTTON6                     1010
#define IDC_BUTTON7                     1011
#define IDC_BUTTON_SET_TRANSPARENT      1011
#define IDC_BUTTON8                     1012
#define IDC_BUTTON9                     1013
#define IDC_BUTTON10                    1014
#define IDC_BUTTON11                    1015
#define IDC_BUTTON_REFRESH              1016
#define IDC_EDIT_SIZE_W                 1017
#define IDC_EDIT_SIZE_H                 1018
#define IDC_BUTTON_SET_SIZE             1019
#define IDC_BUTTON_BRINGTOFRONT_NA      1020
#define IDC_BUTTON_TO_BOTTOM            1021
#define IDC_BUTTON_NOT_ALWAYSONTOP      1022
#define IDC_BUTTON_SET_LOCATION         1023
#define IDC_EDIT1                       1024
#define IDC_EDIT_URL                    1024
#define IDC_EDIT_WINDOW_TITLE           1026
#define IDC_BUTTON12                    1027
#define IDC_BUTTON_RESTORE_NOACTIVATE   1027
#define IDC_BUTTON13                    1028
#define IDC_BUTTON14                    1029
#define IDC_SLIDER_TRANSPARENCY         1030
#define IDC_SLIDER2                     1031

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1032
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
