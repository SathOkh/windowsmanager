#pragma once

#include <vector>
#include <iostream>
using namespace std;

struct WindowInfo 
{
	static const int MAX_LENGTH = 255;

	char windowTitle[MAX_LENGTH];
	DWORD processId;
	char className[MAX_LENGTH];

	HWND hwnd;
};
/**
 *	Wyjatek rzucany przez funkcje tej biblioteki.
 */
class Exception {
		/** Wiadomosc. */
		char* msg;

	public:
		/** 
		 *	Konstruktor.
		 *
		 *	@param msg wiadomosc przypisana do wyjatku
		 */
		Exception(char* msg)
		{
			this->msg = msg;
		}

		/**
		 *	Zwraca wiadomosc przypisana do wyjatku.
		 *
		 *	@return wiadomosc przypisana do wyjatku
		 */
		char* getMsg()
		{
			return msg;
		}
};


vector<WindowInfo> fillWindowsList();
vector<WindowInfo> getChildWindows(HWND parent);
WindowInfo findWindow(DWORD dwProcessId, LPCSTR windowClass);
