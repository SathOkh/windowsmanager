// _WindowsManager_Dlg.h : header file
//

#pragma once
#include "ProcessManager.h"
#include "afxcmn.h"

// CWindowsManager_Dlg dialog
class CWindowsManager_Dlg : public CDialog
{
// Construction
public:
	CWindowsManager_Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_WINDOWSMANAGER__DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	virtual BOOL DestroyWindow();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	void RefreshWindowsList();
	void ClearWindowsData();
	WindowInfo* GetSelectedWindowInfo();
public:
	afx_msg void OnLbnDblclkWindowslist();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnClose();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton9();
	afx_msg void OnBnClickedButton10();
	afx_msg void OnBnClickedButton11();
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg void OnBnClickedButtonSetSize();
private:
	// Szerokosc okna
	int mSizeWidth;
	// Wysokosc okna
	int mSizeHeigh;
	/// Poziom przezroczystosci.
	int m_TransparencyLevel;
	CSliderCtrl m_Transparency;
public:
	afx_msg void OnBnClickedButtonBringtofrontNa();
	afx_msg void OnBnClickedButtonToBottom();
	afx_msg void OnBnClickedButtonNotAlwaysontop();
	afx_msg void OnBnClickedButtonSetLocation();
	CString mIeUrl;
	CString mWindowTitle;
	afx_msg void OnBnClickedButtonSetWindowTitle();
	afx_msg void OnBnClickedButtonRestore();
	afx_msg void OnBnClickedButtonRestoreNoactivate();
	afx_msg void OnBnClickedButton13();
	afx_msg void OnBnClickedButtonSetTransparent();

	afx_msg void OnBnClickedButton14();
};
