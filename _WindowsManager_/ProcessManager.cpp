#include "stdafx.h"

#include <windows.h>
#include <psapi.h>

#include "ProcessManager.h"


BOOL CALLBACK EnumWindowsProc(HWND hWnd, LPARAM lParam) {
	vector<WindowInfo>* windows = (vector<WindowInfo>*)lParam;
	DWORD dwThreadId, dwProcessId;
	HINSTANCE hInstance;
	char String[255];
	HANDLE hProcess;
	WindowInfo info;

	if (!hWnd)
		return TRUE;		// Not a window
	if (!::IsWindowVisible(hWnd))
		return TRUE;		// Not visible
	if (!SendMessage(hWnd, WM_GETTEXT, sizeof(String), (LPARAM)String))
		return TRUE;		// No window title
	info.hwnd = hWnd;
	hInstance = (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE);
	dwThreadId = GetWindowThreadProcessId(hWnd, &dwProcessId);
	hProcess = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION , FALSE, dwProcessId);
	info.processId = dwProcessId;
	//cout << hWnd << ' ' << dwProcessId << '\t' << String << '\t';
	strncpy(info.windowTitle,String, WindowInfo::MAX_LENGTH);
	GetClassName(hWnd, String, 255);
	strncpy(info.className, String, WindowInfo::MAX_LENGTH);
	//cout<<" "<<String<<" ";

	// GetModuleFileNameEx uses psapi, which works for NT only!
	if (::GetModuleFileNameEx(hProcess, hInstance, String, sizeof(String)))
	{
		cout << String << endl;
	}
	else
	{
		cout << "(None)\n";
	}
	CloseHandle(hProcess);
	windows->push_back(info);

	return TRUE;
}

BOOL CALLBACK EnumChildWindowsProc(HWND hWnd, LPARAM lParam) {
	vector<WindowInfo>* windows = (vector<WindowInfo>*)lParam;
	DWORD dwThreadId, dwProcessId;
	HINSTANCE hInstance;
	char String[255];
	HANDLE hProcess;
	WindowInfo info;

	if (!hWnd)
		return TRUE;		// Not a window
	//if (!::IsWindowVisible(hWnd))
	//	return TRUE;		// Not visible
	if (!SendMessage(hWnd, WM_GETTEXT, sizeof(String), (LPARAM)String))
		return TRUE;		// No window title
	info.hwnd = hWnd;
	hInstance = (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE);
	dwThreadId = GetWindowThreadProcessId(hWnd, &dwProcessId);
	hProcess = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, dwProcessId);
	info.processId = dwProcessId;
	cout << hWnd << ' ' << dwProcessId << '\t' << String << '\t';
	strncpy(info.windowTitle,String, WindowInfo::MAX_LENGTH);
	GetClassName(hWnd, String, 255);
	strncpy(info.className, String, WindowInfo::MAX_LENGTH);
	cout<<" "<<String<<" ";

	// GetModuleFileNameEx uses psapi, which works for NT only!
	if (::GetModuleFileNameEx(hProcess, hInstance, String, sizeof(String)))
		cout << String << endl;
	else
		cout << "(None)\n";

	CloseHandle(hProcess);
	windows->push_back(info);

	return TRUE;
}

vector<WindowInfo> fillWindowsList()
{
	vector<WindowInfo> windows;
	EnumWindows(EnumWindowsProc, (LPARAM)&windows);

	return windows;
}

vector<WindowInfo> getChildWindows(HWND parent)
{
	vector<WindowInfo> windows;
	EnumChildWindows(parent, EnumChildWindowsProc, (LPARAM)&windows);
	return windows;
}

WindowInfo findWindow(DWORD dwProcessId, LPCSTR windowClass)
{
	wprintf(L"findWindow: windowClass == %s\n", windowClass);
	HWND currentHWND = NULL;
	vector<WindowInfo> windows = fillWindowsList();

	std::vector<WindowInfo>::iterator it = windows.begin();
	while(it != windows.end() )
	{
		//wprintf(L"findWindow: current classname == %s, pid == %X\n", (*it).className, (*it).processId);
		if((*it).processId == dwProcessId 
			&& 
			(windowClass == NULL || strcmp(windowClass, (*it).className) == 0))
		{
			return (*it);
		}
		it++;
	}
	
	throw Exception("Nie znaleziono takiego okna");
}